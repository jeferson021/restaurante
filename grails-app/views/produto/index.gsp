<!doctype html>
<html>
    <head>
        <meta name="layout" content="main"/>
        <title>Product Manager</title>

        <g:javascript library="jquery" />

        <script type="text/javascript">
            function carregaLista() {
                <g:remoteFunction controller="produto" action="lista" update="divLista" />
            }

            function excluir(id) {
                if(confirm("Deseja Excluir?")) {
                    <g:remoteFunction controller="produto" action="remover" update="divLista" id="'+id+'"/>
                }
            }

            function cancelar() {
                jQuery("#divForm").html("");
            }
        </script>

    </head>

    <body>


    <div id="menu" style="margin: 15px;">
        <g:remoteLink class="btn btn-success" controller="produto" action="adicionar" update="divForm">Cadastrar Produto</g:remoteLink>
    </div>

    <div id="divLista">
        <g:render template="list" model="[produtos: produtos]"></g:render>
    </div>

    <div id="divForm">

    </div>

    </body>

</html>
