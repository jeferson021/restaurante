<g:if test="${produtos.size()>0}"> <!-- Se existir algum produto mostra tabela com os dados  -->

<!-- template para listar as informações do produto -->
<table class="table table-bordered">
    <tr>
        <th>Nome</th>
        <th>Preço</th>
        <th>Qtd Atual</th>
        <th>Qtd Minima</th>
        <th colspan="2">Ações</th>
    </tr>
    <g:each var ="produto" in="${produtos}">
        <tr>
            <td>${produto.nome}</td>
            <td>${produto.preco}</td>
            <td>${produto.estoque?.quantidade}</td>
            <td>${produto.estoque?.quantidadeMinima}</td>
            <td> <g:remoteLink controller="produto" action="alterar" update="divForm" id="${produto.id}"> Alterar </g:remoteLink> </td>
            <td> <a href="#" onclick="excluir(${produto.id})">Excluir</a></td>
        </tr>
    </g:each>
</table>

</g:if>

<g:else>
    Não existem produtos cadastrados!
</g:else>