<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title>Client Manager</title>
</head>

<body>

    <div id="upload-data" class="content" role="main">
        <div class="content" role="main">
            <h1>Upload Data</h1>

            <g:if test="${flash.message}">
                <div class="message" role="alert">
                    ${flash.message}
                </div>
            </g:if>

            <g:uploadForm url="[controller: 'cliente', action: 'doUpload']" update="divLista">
                <fieldset class="form">
                    <input type="file" name="file" />
                </fieldset>
                <fieldset class="buttons">
                    <g:submitButton name="doUpload" value="Upload"/>
                </fieldset>
            </g:uploadForm>

        </div>
    </div>

</body>
</html>