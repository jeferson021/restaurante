package restaurante

class ProdutoController {

    def index() {
        def lista = Produto.list()

        // passando a lista de produtos cadastrados para view
        render(view: "/produto/index", model:[produtos: lista])
    }

    def adicionar() {

        Produto novoProduto = new Produto()
        novoProduto.preco = 0
        novoProduto.estoque = new Estoque()
        novoProduto.estoque.quantidade = 0
        novoProduto.estoque.quantidadeMinima = 0

        render(template: "/produto/form", model:[produto:novoProduto])
    }

    def lista() {
        def lista = Produto.list()
        render(template: "/produto/list", model: [produtos: lista])
    }

    def salvar() {

        Produto produto = null

        if (params.id) { // se existe ID (está alterando)
            produto = Produto.get(params.id)
        } else { // cria novo
            produto = new Produto()
            produto.estoque = new Estoque()
        }

        produto.nome = params.nome
        produto.preco = params.preco.toDouble()
       // produto.estoque = new Estoque()
        produto.estoque.quantidade = params.quantidade.toInteger()
        produto.estoque.quantidadeMinima = params.quantidadeMinima.toInteger()

        produto.validate()
        if(!produto.hasErrors()) {
           produto.save(flush:true)
           render "Cadastro realizado com sucesso!"
        } else {
            render"Ops.. Falha no cadastro!"
        }
    }

    def alterar() {
        Produto produto = Produto.get(params.id)
        render(template: "/produto/form", model: [produto: produto]) //passando pra view
    }

    def remover() {
        Produto produto = null

        if(params.id) {
            produto = Produto.get(params.id)
            produto.delete(flush: true)

            def lista = Produto.list()
            render(template: "/produto/list", model: [produtos: lista])
        }

    }
}
