package restaurante

class ItemPedido {

    Integer quantidade
    Double valorVenda
    String observacao

    Produto produto
    Pedido pedido

    // item pedido pertece a um pedido
    static belongsTo = [Pedido]

    static constraints = {
        valorVenda min: new Double(0)
        observacao nullable:true, blank: true
        quantidade min:0
    }

    static mapping = {
        produto column: "id_produto"
        pedido column: "id_pedido"
    }
}
