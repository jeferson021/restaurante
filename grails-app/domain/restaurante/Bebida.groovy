package restaurante

class Bebida extends Produto {

    Double liquido
    String unidade
    String tipo

    static constraints = {

        liquido min: new Double(0)
        unidade nullable: false, blank: false, inList: ["L", "ml"]
        tipo nullabe: false, blank: false

    }

    static mapping = {
        discriminator value: "BEBIDA"
    }

}
