package restaurante

class Produto {

    String nome
    Double preco
    Estoque estoque

    static hasMany = [clientes: Cliente, itens:ItemPedido]

    //  N * N, porem cliente tem maior importancia
    static belongsTo = Cliente

    static constraints = {
        // Nome não pode ser null, nem ficar em branco //
        nome nullable:false, blank: false
        preco min: new Double(0) // não pode existir preco negativo
    }

    static mapping = {
        // caso nao queira criar uma unica tabela no caso de Herança
        // tablePerHierarchy false

        estoque column: "id_estoque"
        clientes joinTable :[name:"preferecias_clientes", key:"id_produto", column:"id_cliente"]
    }

}
