package restaurante

class Estoque {
    Integer quantidade
    Integer quantidadeMinima

    static belongsTo = Produto

    static constraints = {
        quantidade min:0
        quantidadeMinima:0
    }

    static mapping = {}
}
