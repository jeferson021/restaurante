package restaurante

class Pedido {
    Date dataHora
    Double valorTotal
    String cpf
    Cliente cliente


    static hasMany = [itens:ItemPedido]

    static constraints = {

        dataHora min: new Date() // no mínimo agora
        cliente nullable: false
        cpf maxSize: 11

        // teste simples de validação, mas poderia ser o algoritmo de validacao
        // cpf validator: { valor, objeto -> (valor.size() == 11)}

    }

    static  mapping = {
        cliente column: "id_cliente"
    }
}
